from datetime import datetime
from operator import mod
import os
import sys
import json


# import customtkinter

global base_path
base_path = tmp_global_obj["basepath"]
cur_path = base_path + 'modules' + os.sep + 'workflow-updates' + os.sep + 'libs' + os.sep
sys.path.append(cur_path)

cur_path = base_path + 'modules' + os.sep + 'workflow-updates' + os.sep
sys.path.append(cur_path)

from download import DOWNLOAD_DICT

class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m' 

# Get module
module = GetParams("module")

if module == "update":
    from tkinter import Tk, messagebox
    from tkinter.ttk import Label, Treeview, Button
    from tkinter import Frame
    from PIL import Image, ImageTk

    updates_win = Tk()
    updates_win.title("WorkFlow Updates")
    updates_win.geometry("850x330")
    # updates_win.resizable(0, 0)
    updates_win.iconbitmap(f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}assets{os.sep}256.ico")
    updates_win.configure(bg="#2d2d2d")
    updates_win.title("Updates Panel")

    # Get Workflow Updates Version
    print(os.getcwd())
    with open (f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}package.json", "r") as f:
        data = json.load(f)
        wf_updates_version = data["version"]
    

    # ------------------- HEADER FRAME ------------------- # 
    header_height = int(updates_win.winfo_height() * 0.10) # 10% of the window
    print(f"Header Height: {header_height}")
    frame_bg = "#e1edf5"
    header_frame = Frame(updates_win, 
                        height=header_height,
                        width=updates_win.winfo_screenwidth(),
                        bg=frame_bg,
                        borderwidth=2,
                        relief="flat")
                        
    header_frame.pack(fill="x"
                    )
    # Dentalrobot logo
    logo_image = (
        Image.open(f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}assets{os.sep}256.png")).resize(
        (40, 40), Image.ANTIALIAS)
    logo = ImageTk.PhotoImage(logo_image)
    logo_label = Label(
        header_frame,
        image=logo,
        background=frame_bg,
        foreground="Black",
    )
    logo_label.grid(
        row=0,
        column=0,
        sticky="nsew"
    )
    # Create header label
    header_label = Label(
        header_frame,
        text=f"Workflow Updates v{wf_updates_version}",
        background=frame_bg,
        foreground="Black",
        font=("Noto", 10)
    )
    header_label.grid(
        row=0,
        column=1,
        sticky="nsew"
    )

    # ------------------- TREEVIEW FRAME ------------------- #
    treeview_frame_height = int(updates_win.winfo_height() * 0.80) # 80% of the window
    print(f"Treeview Frame Height: {treeview_frame_height}")
    treeview_frame = Frame(updates_win,
                        height=treeview_frame_height,
                        width=updates_win.winfo_screenwidth(),
                        bg="#2d2d2d",
                        borderwidth=1,
                        relief="flat")
    treeview_frame.pack(
                    fill="both",
                    )
    # Create treeview
    columns = ("id", "module_name", "status", "category")
    treeview = Treeview(
                treeview_frame, 
                columns=columns, 
                show="headings"
            )
    treeview.heading("id", text="#")
    treeview.column("id", stretch=True, width=20)
    treeview.heading("module_name", text="Module Name")
    treeview.heading("status", text="Status")
    treeview.heading("category", text="Category")

    def get_selected_rows():
        import requests
        import zipfile
        import shutil
        import tkinter as tk
        from dentalrobot import version

        global treeview, messagebox, DOWNLOAD_DICT, updates_win, downloading_label
        mods_selected = []
        if len(treeview.selection()) == 0:
            messagebox.showerror("Error", "No modules selected")
        else:
            for item in treeview.selection():
                item_text = treeview.item(item, "values")
                print(item_text)
                mods_selected.append(item_text)

            mods_selected = [list(item)[1] for item in mods_selected]
            mods_selected_str = '\n'.join(mods_selected)
            msg = f"Are you sure you want to update the following modules/libs?\n\n{mods_selected_str}"
            response = messagebox.askyesno("Update Modules", msg)
            
            if response:
                downloading_label.configure(text="Downloading...")
                mods_path = f"{os.getcwd()}/Modules"
                libs_path = f"{mods_path}/libs"
                mods_download_dict = {key:val for key, val in DOWNLOAD_DICT["MODS"].items() if key in mods_selected}
                print(mods_download_dict)
                # The mods are located in differents repositories. On this function we go repo by repo downloading mods
                for mod_name, mod_data in mods_download_dict.items():
                    # download_path is a variable to know if we are going to install a mod or lib, both have diff locations
                    download_path = libs_path if '-lib' in mod_name.lower().strip() else mods_path
                    print(f"\n{'*'*20}{mod_name.upper()} DOWNLOAD{'*'*20}\n\n")
                    url = mod_data["URL"]
                    zip_download_name = mod_data["DOWNLOAD_NAME"]
                    print(f"\nDOWNLOAD NAME: {zip_download_name}\n")
                    response = requests.get(
                                            url=url,
                                            headers={
                                                "private_token":DOWNLOAD_DICT["GITLAB_TOKEN"]
                                                }
                                            )
                    if response.status_code == 200:
                        # Building the zip file with the bytes from our response object
                        with open(f"{download_path}/{zip_download_name}", mode="wb") as zip_data:
                            for chunk in response.iter_content(chunk_size=4096):
                                zip_data.write(chunk)
                        #Unzipping the file
                        with zipfile.ZipFile(f"{download_path}/{zip_download_name}") as zip_data:
                            zip_data.extractall(f"{download_path}")
                
                    # moving the unzipped folder tree to the mods path
                    if mod_data['GITLAB_MOD_NAME']:
                        from_path = f"{download_path}/{mod_data['DOWNLOAD_PATH']}/{mod_data['GITLAB_MOD_NAME']}/"
                        print(f"FROM PATH: {from_path}")
                        print(f"TO PATH: {download_path}/{mod_data['MOD_NAME']}/")
                        # Removing the old folder
                        if os.path.exists(f"{download_path}/{mod_data['MOD_NAME']}/"):
                            shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/")

                        shutil.move(
                            from_path,
                            f"{download_path}/{mod_data['MOD_NAME']}/"
                        )
                        shutil.rmtree(f"{download_path}/{mod_data['DOWNLOAD_PATH']}")
                    else:
                        from_path = f"{download_path}/{mod_data['DOWNLOAD_PATH']}"
                        # Removing the old folder
                        if os.path.exists(f"{download_path}/{mod_data['MOD_NAME']}/"):
                            shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/")

                        shutil.move(
                            from_path,
                            f"{download_path}/{mod_data['MOD_NAME']}"
                        )
                        # shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/{mod_data['MOD_NAME']}-main")
                    
                    # Removing the folder tree generated during the unzipping and the zip file
                    print(f"{download_path}/{mod_data['DOWNLOAD_PATH']}")
                    os.remove(f"{download_path}/{zip_download_name}")
                    
                        
                    path = {'path':{mod_data['GITLAB_MOD_NAME']}}#se define el parametro para la peticion, es el nombre del modulo en gitlab
                    id_git = mod_data['ID_GIT'] #Id del proyecto en gitlab
                    #retorna todos ls commits que se han hecho en el project.
                    inf = requests.get(f'https://gitlab.com/api/v4/projects/{id_git}/repository/commits',
                                                params = path,
                                                headers= {"private-token":DOWNLOAD_DICT["GITLAB_TOKEN"]
                                                          })

                    if inf.status_code == 200:
                        text = inf.text
                        data = json.loads(text)
                        #data[0] es el ultimo cambio que se realizo en el paquete en el repositorio
                        
                        commits = data[0]
                        author_name= commits['author_name']
                        title= commits['title']
                        date_commit = commits['committed_date'].split('T')
                        date_commit = datetime.strptime(date_commit[0], '%Y-%m-%d')
                        date_commit = date_commit.strftime('%d-%m-%Y')
                        
                        #obtener la version dependiendo del tipo de paquete Modulo o libreria
                        if '-lib' in mod_name:
                            updates_version = version
                        else:
                            #Get module version
                            with open (f"{os.getcwd()}\Modules\{mod_data['MOD_NAME']}\package.json", encoding="utf8") as v:
                                data_v = json.load(v)
                                updates_version = data_v["version"]
     
                        #fx********** funcion de la ventana secundaria
                        def n_win():
                            from PIL import Image, ImageTk
                            from tkinter import Frame
                            from tkinter.ttk import Label
                            
                            
                            #comprobamos que la ventana este abierta, en caso de estarlo, para continuar hay que cerrarla
                            
                            
                            n_win.opened = True
                            win_sec = tk.Toplevel()#creamos la venana secundaria 
                            win_sec.transient(updates_win)#la establecemos como una ventana transitoria
                            win_sec.focus()#establecemos el foco sobfre ella
                            win_sec.title('')
                            win_sec.geometry("900x300+500+400")
                            win_sec.configure(bg='white')
                            #"elementos dentro de la tabla"
                            win_sec.iconbitmap(f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}assets{os.sep}256.ico")
                            ##############################
                            header_frame1 = Frame(win_sec, 
                                            width=win_sec.winfo_screenwidth(),
                                            bg="#e1edf5",
                                            borderwidth=2,
                                            relief="flat")
                            
                            header_frame1.pack(padx=5,
                                     pady=5,
                                     ipadx=5,
                                     ipady=5,
                                     fill=tk.X)
                            logo_image1 = (
                                            Image.open(f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}assets{os.sep}256.png")).resize(
                                            (50, 50), Image.ANTIALIAS)
                            logo1 = ImageTk.PhotoImage(logo_image1)
                            
                            logo_label1 = Label(
                                header_frame1,
                                image=logo1,
                                background='#e1edf5',
                                foreground="Black",
                            )
                            logo_label1.grid(
                                row=0,
                                column=0,
                                sticky="nsew"
                            )
                            # Create header label
                            header_label1 = Label(
                                header_frame1,
                                text=f"{mod_name}",
                                background='#e1edf5',
                                foreground="Black",
                                font=("Noto", 10)
                            )
                            header_label1.grid(
                                row=0,
                                column=1,
                                sticky="nsew"
                            )
                            
                            author_N=tk.Label(win_sec,
                                        text=(f"Commit Autor: {author_name}"),
                                        bg = 'white', anchor='w',font=('Helvetica 10'))
                        
                            author_N.pack(padx=5,
                                        pady=5,
                                        ipadx=5,
                                        ipady=5,
                                        fill=tk.X)
                            
                            commit=tk.Label(win_sec,
                                            text=(f"Commit Message: {title}"),
                                            bg = 'white',anchor='w', font=('Helvetica 10'))
                            
                            commit.pack(padx=5,
                                        pady=5,
                                        ipadx=5,
                                        ipady=5,
                                        fill=tk.X)
                            
                            date_C=tk.Label(win_sec,
                                        text=(f"Commit Date: {date_commit}"),
                                        bg = 'white',anchor='w', font=('Helvetica 10'))
                            date_C.pack(padx=5,
                                    pady=5,
                                    ipadx=5,
                                    ipady=5,
                                    fill=tk.X)
                            
                            version_C=tk.Label(win_sec,
                                        text=(f"Version: {updates_version}"),
                                        bg = 'white',anchor='w', font=('Helvetica 10'))
                            version_C.pack(padx=5,
                                    pady=5,
                                    ipadx=5,
                                    ipady=5,
                                    fill=tk.X)
                            #Esperamos a que la ventana se cierre
                            win_sec.wait_window()
                            n_win.opened = False
                        #**************************
                        
                        n_win.opened = False
                        
                        n_win()
                        #updates_win.withdraw()
                        
                    else : 
                        messagebox.showinfo("Success", f"Module {mod_name} no update")                    
                    
                        
                        
            
            updates_win.destroy()
        
    
    # Treeview Items
    for i, mod_data in enumerate(list(DOWNLOAD_DICT["MODS"].items())):
        mod_name, mod_info = mod_data
        mod_path = f"{os.getcwd()}{os.sep}Modules{os.sep}{mod_name}" if mod_name != "dentalrobot-lib" else f"{os.getcwd()}{os.sep}Modules{os.sep}libs{os.sep}{mod_info['MOD_NAME']}"
        status = "Installed" if os.path.exists(mod_path) else "Not Installed"
        cat = "Module" if mod_name != "dentalrobot-lib" else "Python Package"

        treeview.insert("", "end", values=(i+1, mod_name, status, cat))

    treeview.pack(
        fill="both",
        expand=True
    )

    # -------------------DOWNLOAD BUTTON FRAME ------------------- #
    download_button_frame_height = int(updates_win.winfo_height() * 0.10) # 10% of the window
    print(f"Download Button Frame Height: {download_button_frame_height}")
    download_button_frame = Frame(updates_win,
                        height=download_button_frame_height,
                        width=updates_win.winfo_screenwidth(),
                        bg=frame_bg,
                        borderwidth=1,
                        relief="flat"
                    )
    download_button_frame.pack(
                    fill="both",
                    side="bottom",
                    expand=True
                    )
    # button to update selected modules
    download_button = Button(
        download_button_frame,
        text="Download",
        command=get_selected_rows
    )
    download_button.pack(
        side="right",
        padx=10,
        pady=10
    )
    downloading_label = Label(
                            download_button_frame,
                            text="",
                            background=frame_bg,
                            font=('Noto', 10, 'bold'),
                            foreground="Black"
                )
    downloading_label.pack(
        side="left",
        padx=10,
        pady=10
    )


    updates_win.mainloop()

#comando para actualizar modulos desde form experience
#del orquestador center.

elif module == "Orch_update":
    # Get Workflow Updates Version
    with open (f"{os.getcwd()}{os.sep}Modules{os.sep}{GetParams('module_name')}{os.sep}package.json", "r") as f:
        data = json.load(f)
        wf_updates_version = data["version"]

    def get_selected_mods():
        import requests
        import zipfile
        import shutil
        import json
        from dentalrobot import version

        global  DOWNLOAD_DICT, color
        list_mod = eval(GetVar('orch_list_mod'))
        #data= {'success': True, 'data': {'id': 52, 'data': '{"clinic_selected":"Mortenson","OpenDental":"on","dentalrobot-lib":"on"}', 'user_form_email': 'norys.acuna0301@gmail.com'}}
        # mods_download_dict = {key:val for key, val in DOWNLOAD_DICT["MODS"].items() if key in list_mod}
        # print(mods_download_dict)
        print(f"\nlist_mod: {list_mod}\n")
        if list_mod:
                mods_path = f"{os.getcwd()}/Modules"
                libs_path = f"{mods_path}/libs"
                mods_download_dict = {key:val for key, val in DOWNLOAD_DICT["MODS"].items() if key in list_mod}
                print(f"mods_download_dict: {mods_download_dict}")
                # The mods are located in differents repositories. On this loop we go repo by repo downloading mods or libs
                for mod_name, mod_data in mods_download_dict.items():
                    # download_path is a variable to know if we are going to install a mod or lib, both have diff locations
                    download_path = libs_path if '-lib' in mod_name.lower().strip() else mods_path
                    print(f"\n{'*'*20}{mod_name.upper()} DOWNLOAD{'*'*20}\n")
                    url = mod_data["URL"]
                    zip_download_name = mod_data["DOWNLOAD_NAME"]
                    #print(f"\nDOWNLOAD NAME: {zip_download_name}\n")
                    response = requests.get(
                                            url=url,
                                            headers={
                                                "private_token":DOWNLOAD_DICT["GITLAB_TOKEN"]
                                                }
                                            )
                    if response.status_code == 200:
                        # Building the zip file with the bytes from our response object
                        with open(f"{download_path}/{zip_download_name}", mode="wb") as zip_data:
                            for chunk in response.iter_content(chunk_size=4096):
                                zip_data.write(chunk)
                        #Unzipping the file
                        with zipfile.ZipFile(f"{download_path}/{zip_download_name}") as zip_data:
                            zip_data.extractall(f"{download_path}")
                
                    # moving the unzipped folder tree to the mods path
                    if mod_data['GITLAB_MOD_NAME']:
                        from_path = f"{download_path}/{mod_data['DOWNLOAD_PATH']}/{mod_data['GITLAB_MOD_NAME']}/"
                        print(f"FROM PATH: {from_path}")
                        print(f"TO PATH: {download_path}/{mod_data['MOD_NAME']}/")
                        # Removing the old folder
                        if os.path.exists(f"{download_path}/{mod_data['MOD_NAME']}/"):
                            shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/")

                        shutil.move(
                            from_path,
                            f"{download_path}/{mod_data['MOD_NAME']}/"
                        )
                        shutil.rmtree(f"{download_path}/{mod_data['DOWNLOAD_PATH']}")
                    else:
                        from_path = f"{download_path}/{mod_data['DOWNLOAD_PATH']}"
                        # Removing the old folder
                        if os.path.exists(f"{download_path}/{mod_data['MOD_NAME']}/"):
                            shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/")

                        shutil.move(
                            from_path,
                            f"{download_path}/{mod_data['MOD_NAME']}"
                        )
                        # shutil.rmtree(f"{download_path}/{mod_data['MOD_NAME']}/{mod_data['MOD_NAME']}-main")
                    
                    # Removing the folder tree generated during the unzipping and the zip file
                    #print(f"{download_path}/{mod_data['DOWNLOAD_PATH']}")
                    os.remove(f"{download_path}/{zip_download_name}")
                    
                        
                    path = {'path':{mod_data['GITLAB_MOD_NAME']}}#se define el parametro para la peticion, es el nombre del modulo en gitlab
                    id_git = mod_data['ID_GIT'] #Id del proyecto en gitlab
                    #retorna todos ls commits que se han hecho en el project.
                    inf = requests.get(f'https://gitlab.com/api/v4/projects/{id_git}/repository/commits',
                                                params = path,
                                                headers= {"private-token":DOWNLOAD_DICT["GITLAB_TOKEN"]
                                                          })

                    if inf.status_code == 200:
                        text = inf.text
                        data = json.loads(text)
                        #data[0] es el ultimo cambio que se realizo en el paquete en el repositorio
                        
                        commits = data[0]
                        author_name= commits['author_name']
                        title= commits['title']
                        date_commit = commits['committed_date'].split('T')
                        date_commit = datetime.datetime.strptime(date_commit[0], '%Y-%m-%d')
                        date_commit = date_commit.strftime('%d-%m-%Y')
                        
                        #obtener la version dependiendo del tipo de paquete Modulo o libreria
                        if '-lib' in mod_name:
                            updates_version = version
                        else:
                            #Get module version
                            with open (f"{os.getcwd()}\Modules\{mod_data['MOD_NAME']}\package.json", encoding="utf8") as v:
                                data_v = json.load(v)
                                updates_version = data_v["version"]
     
                        print(f"{color.GREEN}Commit Autor:   {color.CYAN}{author_name}")
                        print(f"{color.GREEN}Commit Message: {color.CYAN}{title}")
                        print(f"{color.GREEN}Commit Date:    {color.CYAN}{date_commit}")
                        print(f"{color.GREEN}Version:        {color.CYAN}{updates_version}{color.END}")    
    get_selected_mods()                   