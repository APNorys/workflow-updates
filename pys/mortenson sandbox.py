from base64 import b64encode
import pandas as pd
from datetime import datetime
from hashlib import sha512
from hmac import new
from requests import get, post
from dentalrobot.dentalrobot import getbookmarks, get_field_value, get_bookmark_value
from time import sleep
import json
import re
import zipfile
import numpy as np


def clean_deductible(deductible_amount):
    print(f'DEDUCTIBLE AMOUNT DATA IN PATIENT FORM BEFORE CLEANING ==> {deductible_amount}')
    # Validating if deductible amount has a value
    if str(deductible_amount).strip() not in [' ', '', '-', 'None', None, '', '$NaN', 'NaN', 'info not available',
                                            'This program has no deductibles.','not covered', 'not applicable'] and bool(
        re.match(r'.*\d+.*', str(deductible_amount))):
        # Cleaning up the deductible amount
        deductible_amount = float(str(deductible_amount).replace(',', '').replace('$', ''))
        return deductible_amount
    else:
        return 0
    
def process_age_limit(age_limit):
    print(f'\nAGE LIMIT DATA IN PATIENT FORM RAW ==> {age_limit}')
    age_limit = age_limit.lower().replace('child up to and not including age 16, child up to and not including age 9', 'age 8')
    # Validating if age limit has a value
    if age_limit != '-' and age_limit != None and age_limit != 'None' and age_limit != '' and age_limit != -1:
        # Cleaning up the age limit
        age_limit = age_limit.replace('+', '')
        age_limit_description = age_limit
        age_limit = [int(i) for i in age_limit.split(f'{"-" if "-" in age_limit else " "}') if i.strip().isdigit()]
        if len(age_limit) != 0:
            age_limit = age_limit[0] if len(age_limit) == 1 else age_limit[1]
            age_limit = float(age_limit) - 1 if 'up to' in age_limit_description.lower() else age_limit
            print(f'\nAGE LIMIT AFTER PROCESSING==> {age_limit}')
            return age_limit
        else:
            return 0
    else:
        return 0
    
def process_percentage(percent):
    print(f"PERCENT WITHOUT PROCESSING: {percent}")
    if percent.lower().strip() in ["not applicable", "not covered"]:
        return 0
    else:
        return percent.replace("%", "")
    
def process_xray(titles, values):
    global get_field_value
    if "%" in get_field_value(titles, values, "text", bookmark_name="D0220_Percentage"):
        x_ray_cov = get_field_value(titles, values, "text", bookmark_name="D0220_Percentage", default_return="")
    elif "%" in get_field_value(titles, values, "text", bookmark_name="D0274_Percentage"):
        x_ray_cov = get_field_value(titles, values, "text", bookmark_name="D0274_Percentage", default_return="")
    else:
        x_ray_cov = get_field_value(titles, values, "text", bookmark_name="D0330_Percentage", default_return="")
    
    return x_ray_cov

def is_there_a_cov(coverage):
    if "%" in coverage:
        return "Yes"
    elif "not covered" in coverage.lower().strip():
        return "No"
    else:
        return ""
    
def d9230Restrictions(titles, values, restriction):
    print(f"\nRESTRICTION D9230: {restriction}")
    global get_field_value
    if restriction in ["-", ""]:
        restriction_value = "Not covered" if (get_field_value(titles, values, "text", bookmark_name="D9230frequency").lower().strip() == "not covered" or get_field_value(titles, values, "text", bookmark_name="D9230_Percentage").lower().strip() == "not covered") else restriction
        return restriction_value
    else:
        return restriction
    
def proccess_bruxism(titles, values):
    global get_field_value
    bruxism_coverage = get_field_value(titles, values, type="text", bookmark_name="D9944_Percentage")

    return "No" if "not covered" in bruxism_coverage.lower().strip() else "Yes"


hmac_secret = "wf8KSj1ZSNSWOrvJY6tU4AOL62AKz4yc"
hmac_user = "mortenson"
apiKey = "k3PKsmQthZOZNVkPwQ76RhCSgjKcmVFB"

dateUTC = datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")
stringToSign = f"date: {dateUTC}\nhost: cluster.dentalrobot.co\napiKey: {apiKey}"
hmac = b64encode(new(bytes(hmac_secret, "UTF-8"),
                bytes(stringToSign, "UTF-8"), sha512).digest()).decode()
authorization = f'hmac username="{hmac_user}", algorithm="hmac-sha512", headers="date host apiKey", signature="{hmac}" '
headers = {
    "apiKey": apiKey,
    "authorization": authorization,
    "date": dateUTC
}

print(f"Header: {json.dumps(headers, indent=4)}")
endpoint = "https://cluster.dentalrobot.co/v1/bot/verification"


try:
    titles, values, form_status = getbookmarks(GetVar("docx_path"))
except:
    print("Theres no form")

today_date = datetime.today().strftime("%Y-%m-%d")

verification_status = GetVar("verification_status")

zip_code = int(GetVar("sub_zipcode")) if GetVar("sub_zipcode") not in ["Empty", "", " "] else GetVar("sub_zipcode")
patNum = int(GetVar("patnum")) if GetVar("patnum") not in ["Empty", "", " "] else GetVar("patnum")


if GetVar("verificationType") == "FBD" and GetVar("verification_status").split("/")[0].strip() == "Active" and 'docs.google.com' in GetVar("file_url"):
    from dentalrobot.frequencies import translate_values
    import yaml

    try:
        with open(GetVar("yaml_file")) as y:
            config = yaml.load(y, Loader=yaml.FullLoader)
    except Exception as e:
        raise Exception(f"\nError while opening config file.\nError Details: {e}\n")

    frequencies_values = []
    if True:
        print(f'{"*" * 40} USING CODE LIST {"*" * 40}')
        columns = ["Bookmark", "Description", "BenefitType",
                "TimePeriod", "QuantityQualifier", "Quantity", "CovCatNum",
                "CodeNum", "OldValue", "Exist"]

        data = config["frequencies_code_list"]
        bookmarks = [item[0] for item in data]
        patient_df = pd.DataFrame(data, columns=columns, index=bookmarks)
        #  VALIDATING IF THERE A FREQUENCY
        if len(patient_df.values.tolist()) > 0:
            for bookmark in patient_df.index.tolist():
                value = get_bookmark_value(titles, values, bookmark)
                value = str(value).lower()
                frequencies_values.append(value)

            patient_df["Bookmark_values"] = frequencies_values
            # DROPING NOT COVERED AND -
            # patient_df = patient_df.drop(patient_df[(patient_df['Bookmark_values'] == 'not covered') | (
            #             patient_df['Bookmark_values'] == 'not applicable') | (patient_df['Bookmark_values'] == '-') | (
            #                                                     patient_df['Bookmark_values'] == '-1') | (
            #                                                     patient_df['Bookmark_values'] == -1)].index)
            patient_df.drop_duplicates(subset="Bookmark", inplace=True)

        print(patient_df)
        # Data tranlslated, ready to update
        data_translated = translate_values(patient_df)
        data_translated.loc[data_translated["Type"] == 1, "QuantityQualifier"] = "Every # Months"
        data_translated.loc[data_translated["Type"] == 2, "QuantityQualifier"] = "# Per Benefit Year"
        data_translated.loc[data_translated["Type"] == 3, "QuantityQualifier"] = "# in Last 12 months"
        data_translated.loc[data_translated["Type"] == 4, "QuantityQualifier"] = "Every # Years"

        # Life time
        data_translated.loc[data_translated["Type"] == 5, "QuantityQualifier"] = "Every # Years"
        data_translated.loc[data_translated["Type"] == 5, "Quantity"] = 99

        # Not found
        data_translated.loc[data_translated["Type"] == "Not Found", "Quantity"] = 0
        data_translated.loc[data_translated["Type"] == "Not Found", "QuantityQualifier"] = ""

        # Dashes
        data_translated.loc[data_translated["Bookmark_values"] == "-", "Quantity"] = 0
        data_translated.loc[data_translated["Bookmark_values"] == "-", "QuantityQualifier"] = "# Per Benefit Year"
        
        # Not covered
        data_translated.loc[data_translated["Bookmark_values"] == "not covered", "Quantity"] = 0
        data_translated.loc[data_translated["Bookmark_values"] == "not covered", "QuantityQualifier"] = "# Per Benefit Year"

        # No time limits
        data_translated.loc[data_translated["Bookmark_values"] == "no frequency time limitations", "Quantity"] = 99
        data_translated.loc[data_translated["Bookmark_values"] == "no frequency time limitations", "QuantityQualifier"] = "# Per Benefit Year"

        data_translated.loc[data_translated["Bookmark_values"].str.contains('no time limit'), "Quantity"] = 99
        data_translated.loc[data_translated["Bookmark_values"].str.contains('no time limit'), "QuantityQualifier"] = "# Per Benefit Year"


        cols = ["Bookmark", "QuantityQualifier"]

        df_print = data_translated[cols]

        print(df_print)

        # Delta Toolkit Defaults
        limitation_xray = get_field_value(titles, values, "text", bookmark_name="D0220_HowmanyPAbeforeconsideredFMX", default_return="")
        d2930_qualifier = data_translated.loc['D2930frequency']['QuantityQualifier']
        d2930_quatity = data_translated.loc['D2930frequency']['Quantity'] if data_translated.loc['D2930frequency']['Quantity'] != "-" else 0
        d2931_qualifier = data_translated.loc['D2931frequency']['QuantityQualifier']
        d2931_quatity = data_translated.loc['D2931frequency']['Quantity'] if data_translated.loc['D2931frequency']['Quantity'] != "-" else 0
        wait_period = get_field_value(titles, values, "checkbox", checkboxes={"IsthereanywaitingPeriodYES": "Next Day", "IsthereanywaitingPeriodNO": "No"}, default_return="")
        composite_quantity = data_translated.loc['D2391frequency']['Quantity'] if  data_translated.loc['D2391frequency']['Quantity'] != "-" else 0

        carrier_name = GetVar("carrier_name")
        config = eval(GetVar("configP")) if GetVar("configP") else GetVar("configP")

        if re.search(config["Delta Dental Toolkit API"]["regex"], carrier_name.strip(), re.IGNORECASE):
            limitation_xray = limitation_xray if limitation_xray not in ["-", "", " "] else "14 or more films"

            d2930_qualifier = data_translated.loc['D2930frequency']['QuantityQualifier'] if data_translated.loc['D2930frequency']['QuantityQualifier'] not in ["-", "", " ", "# Per Benefit Year"] else "Every # Months"
            d2930_quatity = data_translated.loc['D2930frequency']['Quantity'] if data_translated.loc['D2930frequency']['Quantity'] not in ["-", "", " ", 0] else 24

            d2931_qualifier = data_translated.loc['D2931frequency']['QuantityQualifier'] if data_translated.loc['D2931frequency']['QuantityQualifier'] not in ["-", "", " ", "# Per Benefit Year"] else "Every # Months"
            d2931_quatity = data_translated.loc['D2931frequency']['Quantity'] if data_translated.loc['D2931frequency']['Quantity'] not in ["-", "", " ", 0] else 24

            wait_period = get_field_value(titles, values, "checkbox", checkboxes={"IsthereanywaitingPeriodYES": "Next Day", "IsthereanywaitingPeriodNO": "No"}, default_return="Next Day")
            composite_quantity = data_translated.loc['D2391frequency']['Quantity'] if  data_translated.loc['D2391frequency']['Quantity'] not in ["-", "", " ", 0] else 24


    body = [{
        "locId": int(GetVar("loc_id")),
        "aptNum": int(GetVar("apt_num")),
        "carrierName": GetVar("carrier_name"),
        "memberId": GetVar("member_Id"),
        "groupNum": GetVar("pms_group_num"),
        "confirmGroupNum": GetVar("web_group_num") if GetVar("web_group_num") not in ['', ' ', '  ', '   ', '-', 'Empty'] else GetVar("pms_group_num"),
        "subscriberFirstName": GetVar("sub_fname"),
        "subscriberLastName": GetVar("sub_fname"),
        "subscriberZipCode": int(GetVar("sub_zipcode")) if GetVar("sub_zipcode") not in ["Empty", "", " "] else GetVar("sub_zipcode"),
        "subscriberDOB": GetVar("sub_birthday"),
        "patientFirstName": GetVar("fname"),
        "patientLastName": GetVar("lname"),
        "patientDOB": GetVar("pat_birthday"),
        "relationship": GetVar("sub_relationship"),
        "verificationType": GetVar("verificationType"),
        "verificationStatus": 0,  # 1 -> Active: Active policy --- 2 -> Inactive: Inactive policy --- 3 -> Not Found: The patient was not found on Carrier's website, --- 4 -> No Content Loaded: The site did not load an element. --- 5 -> Max out: Active policy, but maxed out. --- 6 -> Termed: The insurance plan has ended. --- 7 -> Unknown: There is no information for a Dental Plan.
        "patientId": int(GetVar("patnum")) if GetVar("patnum") not in ["Empty", "", " "] else GetVar("patnum"),
        "subscriberId": int(GetVar("sub_id")) if GetVar("sub_id") not in ["Empty", "", " "] else GetVar("sub_id"),
        "planId": int(GetVar("plannum")) if GetVar("plannum") not in ["Empty", "", " "] else GetVar("plannum"),
        "planStartDate": str(GetVar("plan_start_date")).split("-")[0].strip() if '-' in GetVar("plan_start_date") else "",
        "appointmentDateTime": GetVar("app_datetime"),
        "employerName": GetVar("employer_name"),
        "priority": int(GetVar("priority")) if GetVar("priority") not in ["Empty", "", " "] else GetVar("priority"),
        "benefitLastVerified": today_date, 
        "eligibilityLastVerified": today_date, 
        "benefitVerifiedBreakdown": {
            "sec1": {
                "compExams": data_translated.loc['D0150frequency']['QuantityQualifier'],
                "compExamsQuantity": data_translated.loc['D0150frequency']['Quantity'] if data_translated.loc['D0150frequency']['Quantity'] != "-" else 0,
                "periodicExams": data_translated.loc['D0120frequency']['QuantityQualifier'],
                "periodicExamsQuantity": data_translated.loc['D0120frequency']['Quantity'] if data_translated.loc['D0120frequency']['Quantity'] != "-" else 0,
                "limitedExams": data_translated.loc['D0140frequency']['QuantityQualifier'],
                "limitedExamsQuantity": data_translated.loc['D0140frequency']['Quantity'] if data_translated.loc['D0140frequency']['Quantity'] != "-" else 0,
                "bwxFequency": data_translated.loc['D0272frequency']['QuantityQualifier'],
                "bwxFequencyQuantity": data_translated.loc['D0272frequency']['Quantity'] if data_translated.loc['D0272frequency']['Quantity'] != "-" else 0,
                "panoFMXFrequency": data_translated.loc['D0210frequency']['QuantityQualifier'],
                "panoFMXFrequencyQuantity": data_translated.loc['D0210frequency']['Quantity'] if data_translated.loc['D0210frequency']['Quantity'] != "-" else 0,
                "paFrequency": data_translated.loc['D0220frequency']['QuantityQualifier'] if data_translated.loc['D0220frequency']['QuantityQualifier'] else "# Per Benefit Year",
                "paFrequencyQuantity": data_translated.loc['D0220frequency']['Quantity'] if data_translated.loc['D0220frequency']['Quantity'] not in ["-", "", " ", 0] else 99,
                "OcclusalFrequency": data_translated.loc['D0240frequency']['QuantityQualifier'] if data_translated.loc['D0240frequency']['QuantityQualifier'] else "# Per Benefit Year", 
                "OcclusalFrequencyQuantity": data_translated.loc['D0240frequency']['Quantity'] if data_translated.loc['D0240frequency']['Quantity'] not in ["-", "", " ", 0] else 99,
                "fmx": get_field_value(titles, values, "checkbox", checkboxes={"D0240_sharefreqD0210": "Yes", "D0240_doesnotsharefreq": "No"}, default_return="Yes"),
                "limitationXrays": limitation_xray,
                "deductibleApplyExams": get_field_value(titles, values, "checkbox", checkboxes={"D0120_DeductibleYES": "Yes", "D0120_DeductibleNO": "No"}, default_return="No"),
                "deductibleApplyXrays": get_field_value(titles, values, "checkbox", checkboxes={"D0272_DeductibleYES": "Yes", "D0272_DeductibleNO": "No"}, default_return="No"),
                "prophyFrequency": data_translated.loc['D1110frequency']['QuantityQualifier'],
                "prophyFrequencyQuantity": data_translated.loc['D1110frequency']['Quantity'] if data_translated.loc['D1110frequency']['Quantity'] != "-" else 0,
                "fluorideFrequency": data_translated.loc['D1206frequency']['QuantityQualifier'],
                "fluorideFrequencyQuantity": data_translated.loc['D1206frequency']['Quantity'] if data_translated.loc['D1206frequency']['Quantity'] != "-" else 0,
                "ageLimitFluoride": process_age_limit(get_field_value(titles, values, "text", bookmark_name="D1206_AgeLimit", default_return="")),
                "cancerScreening": is_there_a_cov(get_field_value(titles, values, "text", bookmark_name="D0431_Percentage", default_return="")),
                "cancerScreeningFrequency": data_translated.loc['D0431frequency']['QuantityQualifier'],
                "cancerScreeningFrequencyQuantity": data_translated.loc['D0431frequency']['Quantity'] if data_translated.loc['D0431frequency']['Quantity'] != "-" else 0,
                "spaceMaintFrequency": data_translated.loc['D1520frequency']['QuantityQualifier'] if data_translated.loc['D1520frequency']['QuantityQualifier'] else "# Per Benefit year",
                "spaceMaintFrequencyQuantity": data_translated.loc['D1520frequency']['Quantity'] if data_translated.loc['D1520frequency']['Quantity'] != "-" else 99,
                "ageLimitSpaceMaint": process_age_limit(get_field_value(titles, values, "text", bookmark_name="D1520_AgeLimit", default_return="")),
                "sealantFrequency": data_translated.loc['D1351frequency']['QuantityQualifier'],
                "sealantFrequencyQuantity": data_translated.loc['D1351frequency']['Quantity'] if data_translated.loc['D1351frequency']['Quantity'] != "-" else 0,
                "ageLimitSealant": process_age_limit(get_field_value(titles, values, "text", bookmark_name="D1351_AgeLimit", default_return="")),
                "coveredSealantTeeth": get_field_value(titles, values, "text", bookmark_name="D1351_Specificteethcov", default_return=""),
                "d1354Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D1354_Percentage", default_return="")), 
                "d1516Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D1516_Percentage", default_return="")), 
                "d1517Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D1517_Percentage", default_return=""))
                },
            "sec2": {
                "compositesDowngrade": get_field_value(titles, values, "checkbox", checkboxes={"D2391_DowngradeYES": "Yes", "D2391_DowngradeNO": "No"}, default_return="") ,
                "compositeFrequency": data_translated.loc['D2391frequency']['QuantityQualifier'] ,
                "compositeFrequencyQuantity": composite_quantity,
                "srpFrequency": data_translated.loc['D4341frequency']['QuantityQualifier'] ,
                "srpFrequencyQuantity": data_translated.loc['D4341frequency']['Quantity'] if  data_translated.loc['D4341frequency']['Quantity'] != "-" else 0,
                "srpNumber": get_field_value(titles, values, "checkbox", checkboxes={"D4341_4quadsYES": "4", "D4341_4quadsNO": "2"}, default_return="2") ,
                "waitPeriod": "Next Day",
                "d4910Frequency": data_translated.loc['D4910frequency']['QuantityQualifier'],
                "d4910FrequencyQuantity": data_translated.loc['D4910frequency']['Quantity'] if data_translated.loc['D4910frequency']['Quantity'] != "-" else 0,
                "performD4910": get_field_value(titles, values, "text", bookmark_name="D4910_NumberofdayafterSRP", default_return="30+ Days"),
                "d4910Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D4910_Percentage", default_return="")),
                "d4910Prophy": get_field_value(titles, values, "checkbox", checkboxes={"D4910_sharefreqD1110": "Yes", "D4910_doesnotsharefreq": "No"}, default_return=""),
                "d4910Deductable": get_field_value(titles, values, "checkbox", checkboxes={"D4910_DeductibleYES": "Yes", "D4910_DeductibleYES": "No"}, default_return=""), 
                "d4346Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D4346_Percentage", default_return="")),
                "d4346SharesFreq": get_field_value(titles, values, "checkbox", checkboxes={"D4346_sharefreqD1110": "Yes", "D4346_doesnotsharefreq": "No"}, default_return="Yes"),
                "fmdFrequency": data_translated.loc['D4355frequency']['QuantityQualifier'] if data_translated.loc['D4355frequency']['QuantityQualifier'] else "Every # Years",
                "fmdFrequencyQuantity": data_translated.loc['D4355frequency']['Quantity'] if data_translated.loc['D4355frequency']['Quantity'] not in ["-", "", " ", 0] else 99,
                "d9230Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9230_Percentage", default_return="")),
                "d9230Restrictions": d9230Restrictions(titles, values, get_field_value(titles, values, "text", bookmark_name="D9230_Limitation", default_return="")),
                "surgicalExtractions": get_field_value(titles, values, "checkbox", checkboxes={"D7140_DifferentPercentage_D7210YES": "Yes", "D7140_DifferentPercentage_D7210NO": "No"}, default_return=""),
                "d7210Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D7210_Percentage", default_return="")),
                "d7140Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D7140_Percentage", default_return="")),
                "d9944Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9944_Percentage", default_return="")),
                "coveredBruxism": proccess_bruxism(titles, values),
                "d7961Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D7961_Percentage", default_return="")), 
                "d7962Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D7962_Percentage", default_return="")), 
                "d9248Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9248_Percentage", default_return="")), 
                "d3120Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D3120_Percentage", default_return="")), 
                "d3220Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D3220_Percentage", default_return="")), 
                "d9420Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9420_Percentage", default_return=""))
                },
            "sec3": {
                "replaceTime": data_translated.loc['ReplacementClauseCrown']['Quantity'] if data_translated.loc['ReplacementClauseCrown']['Quantity'] != "-" else 0,
                "replacementFrequency": data_translated.loc['ReplacementClauseCrown']['QuantityQualifier'].replace('Every # Years', '1-Every # Years').replace('Every # Months', '2-Every # Months'),
                "prostheticsReplaceTime": data_translated.loc['ReplacementClauseProsthetic']['Quantity'] if data_translated.loc['ReplacementClauseProsthetic']['Quantity'] != "-" else 0,
                "prostheticsReplaceTimeFrequency": data_translated.loc['ReplacementClauseProsthetic']['QuantityQualifier'], 
                "prepSeat": get_field_value(titles, values, "checkbox", checkboxes={"D2740_PaymentbasedonPrepDate": "2-Prep", "D2740_PaymentbasedonSeatDate": "1-Seat"}, default_return="1-Seat"), 
                "missingTeeth": get_field_value(titles, values, "checkbox", checkboxes={"MissingToothClauseYES": "Yes", "MissingToothClauseNO": "No"}, default_return=""), 
                # "codeCrownsDowngrade": get_field_value(titles, values, "text", bookmark_name="D2740_downgradedto", default_return="D2751") if get_field_value(titles, values, "text", bookmark_name="D2740_downgradedto", default_return="D2751") not in ["-", "", " "] else "D2751",
                # "crownsDowngradeTeeth": "4-Posterior", 
                "recementFrequency": data_translated.loc['D2920frequency']['QuantityQualifier'] if data_translated.loc['D2920frequency']['QuantityQualifier'] else "Every # Years", 
                "recementFrequencyQuantity": data_translated.loc['D2920frequency']['Quantity'] if data_translated.loc['D2920frequency']['Quantity'] != 0 else 99, 
                "coreBuildupsFreq": data_translated.loc['D2950frequency']['QuantityQualifier'], 
                "coreBuildupsFreqQuantity":  data_translated.loc['D2950frequency']['Quantity'] if data_translated.loc['D2950frequency']['Quantity'] != "-" else 0, 
                "d2930Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D2930_Percentage", default_return="")),
                "d2930Freq": d2930_qualifier,
                "d2930FreqQuantity": d2930_quatity,
                "d2931Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D2931_Percentage", default_return="")),
                "d2931Freq": d2931_qualifier,
                "d2931FreqQuantity": d2931_quatity,
                "coveragePercent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D5730_Percentage", default_return="")),
                "waitPeriodAfterDelivery": get_field_value(titles, values, "text", bookmark_name="D5731_Howlongafterdelivery", default_return="6+ Months") if get_field_value(titles, values, "text", bookmark_name="D5731_Howlongafterdelivery", default_return="6+ Months") not in ["-", ""] else "6+ Months",
                # "codeBridgesDowngrade": "D6240", # Qué es esto?
                # "bridgesDowngradeTeeth": "3-Never", # Qué es esto?
                "D6010Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D6010_Percentage", default_return="")),
                "D6010Frequency": data_translated.loc['D6010frequency']['QuantityQualifier'],
                "D6010FrequencyQuantity": data_translated.loc['D6010frequency']['Quantity'] if data_translated.loc['D6010frequency']['Quantity'] != "-" else 0,
                "D6058Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D6058_Percentage", default_return="")),
                "D6058Frequency": data_translated.loc['ReplacementClauseCrown']['QuantityQualifier'],
                "D6058FrequencyQuantity": data_translated.loc['ReplacementClauseCrown']['Quantity'] if data_translated.loc['ReplacementClauseCrown']['Quantity'] != "-" else 0,
                "D6058DowngradeTo": get_field_value(titles, values, "text", bookmark_name="D6058_downgradedto", default_return=""),
                # "D6058DowngradeTeeth": "3-Never", # No existe
                "D6057Covered": is_there_a_cov(get_field_value(titles, values, "text", bookmark_name="D6057_Percentage", default_return="")), 
                "D9910Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9911_Percentage", default_return="")), 
                "D9910Frequency":  data_translated.loc['D9910frequency']['QuantityQualifier'], 
                "D9910FrequencyQuantity": data_translated.loc['D9910frequency']['Quantity'] if data_translated.loc['D9910frequency']['Quantity'] != "-" else 0, 
                "D9911Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D9911_Percentage", default_return="")),
                "D9911Frequency": data_translated.loc['D9911frequency']['QuantityQualifier'],
                "D9911FrequencyQuantity": data_translated.loc['D9911frequency']['Quantity'] if data_translated.loc['D9911frequency']['Quantity'] != "-" else 0,
                "d7953Percent": process_percentage(get_field_value(titles, values, "text", bookmark_name="D7953_Percentage", default_return=""))
                },
            "sec4": {
                "orthoMax": get_field_value(titles, values, "text", bookmark_name="LifetimeMax", default_return="").replace("$", "").replace(",", "")
                ,
                "orthoPercent": process_percentage(get_field_value(titles, values, "text", bookmark_name="Orthodontic", default_return="")),
                "orthoAgeLimit": process_age_limit(get_field_value(titles, values, "text", bookmark_name="Orthodontics_AgeLimit", default_return="")) - 1 if process_age_limit(get_field_value(titles, values, "text", bookmark_name="Orthodontics_AgeLimit", default_return="")) != 0  else process_age_limit(get_field_value(titles, values, "text", bookmark_name="Orthodontics_AgeLimit", default_return=""))
                },
            "sec5": {
                "Prev": process_percentage(get_field_value(titles, values, "text", bookmark_name="Preventive", default_return=""))
                ,
                "Diag": process_percentage(get_field_value(titles, values, "text", bookmark_name="Diagnostic", default_return=""))
                ,
                "Basic": process_percentage(get_field_value(titles, values, "text", bookmark_name="Basic", default_return=""))
                ,
                "Major": process_percentage(get_field_value(titles, values, "text", bookmark_name="Major", default_return=""))
                ,
                "waitPeriodPlan": get_field_value(titles, values, "checkbox", checkboxes={"IsthereanywaitingPeriodYES": "Yes", "IsthereanywaitingPeriodNO": "No"}, default_return="")
                },
            "sec6": {
                "Endo": get_field_value(titles, values, "checkbox", checkboxes={"D3310_Basic": "Basic", "D3310_Major": "Major"}, default_return="No Coverage")
                ,
                "Perio": get_field_value(titles, values, "checkbox", checkboxes={"D4341_Basic": "Basic", "D4341_Major": "Major"}, default_return="No Coverage")
                ,
                "OralSurgery": get_field_value(titles, values, "checkbox", checkboxes={"D7210_Basic": "Basic", "D7210_Major": "Major"}, default_return="No Coverage")
                ,
                "prosthPercent": process_percentage(get_field_value(titles, values, "text", bookmark_name="Prosthodontics", default_return=""))
                ,
                "xrayPercent": process_percentage(process_xray(titles, values))
                },
            "sec7": {
                "yrmax": get_field_value(titles, values, "text", bookmark_name="AnnualMaximum", default_return="").replace("$", "").replace(",", "").replace("None", "0").replace("-", "0") 
                ,
                "famded": str(clean_deductible(get_field_value(titles, values, "text", bookmark_name="Family", default_return=""))),
                
                "indded": clean_deductible(get_field_value(titles, values, "text", bookmark_name="Deductible", default_return=""))
                }
            },
    "driveFileIds":[
            {
            "googleId": GetVar("file_url").split("/")[-2],
            "type": GetVar("verificationType").lower()
            }
        ],
    "rowNumber": GetVar("row_number")
    }]

elif GetVar("file_url") != "Empty":

    body = [{
        "locId": int(GetVar("loc_id")),
        "aptNum": int(GetVar("apt_num")),
        "carrierName": GetVar("carrier_name"),
        "memberId": GetVar("member_Id"),
        "groupNum": GetVar("pms_group_num"),
        "confirmGroupNum": GetVar("web_group_num") if GetVar("web_group_num") not in ['', ' ', '  ', '   ', '-', 'Empty'] else GetVar("pms_group_num"),
        "subscriberFirstName": GetVar("sub_fname"),
        "subscriberLastName": GetVar("sub_fname"),
        "subscriberZipCode": int(GetVar("sub_zipcode")) if GetVar("sub_zipcode") not in ["Empty", "", " "] else GetVar("sub_zipcode"),
        "subscriberDOB": GetVar("sub_birthday"),
        "patientFirstName": GetVar("fname"),
        "patientLastName": GetVar("lname"),
        "patientDOB": GetVar("pat_birthday"),
        "relationship": GetVar("sub_relationship"),
        "verificationType": GetVar("verificationType"),
        "verificationStatus": 0,  # 1 -> Active: Active policy --- 2 -> Inactive: Inactive policy --- 3 -> Not Found: The patient was not found on Carrier's website, --- 4 -> No Content Loaded: The site did not load an element. --- 5 -> Max out: Active policy, but maxed out. --- 6 -> Termed: The insurance plan has ended. --- 7 -> Unknown: There is no information for a Dental Plan.
        "patientId": int(GetVar("patnum")) if GetVar("patnum") not in ["Empty", "", " "] else GetVar("patnum"),
        "subscriberId": int(GetVar("sub_id")) if GetVar("sub_id") not in ["Empty", "", " "] else GetVar("sub_id"),
        "planId": int(GetVar("plannum")) if GetVar("plannum") not in ["Empty", "", " "] else GetVar("plannum"),
        "planStartDate": str(GetVar("plan_start_date")).split("-")[0].strip() if '-' in GetVar("plan_start_date") else "",
        "appointmentDateTime": GetVar("app_datetime"),
        "employerName": GetVar("employer_name"),
        "priority": int(GetVar("priority")) if GetVar("priority") not in ["Empty", "", " "] else GetVar("priority"),
        "benefitLastVerified": "",#today ,
        "eligibilityLastVerified": today_date, ##today
        "benefitVerifiedBreakdown": {},
    "driveFileIds":[
            {
            "googleId": GetVar("file_url").split("/")[-2],
            "type": GetVar("verificationType").lower()
            }
        ],
    "rowNumber": GetVar("row_number")
    }]

else:
    body = [{
        "locId": int(GetVar("loc_id")),
        "aptNum": int(GetVar("apt_num")),
        "carrierName": GetVar("carrier_name"),
        "memberId": GetVar("member_Id"),
        "groupNum": GetVar("pms_group_num"),
        "confirmGroupNum": GetVar("web_group_num") if GetVar("web_group_num") not in ['', ' ', '  ', '   ', '-', 'Empty'] else GetVar("pms_group_num"),
        "subscriberFirstName": GetVar("sub_fname"),
        "subscriberLastName": GetVar("sub_fname"),
        "subscriberZipCode": int(GetVar("sub_zipcode")) if GetVar("sub_zipcode") not in ["Empty", "", " "] else GetVar("sub_zipcode"),
        "subscriberDOB": GetVar("sub_birthday"),
        "patientFirstName": GetVar("fname"),
        "patientLastName": GetVar("lname"),
        "patientDOB": GetVar("pat_birthday"),
        "relationship": GetVar("sub_relationship"),
        "verificationType": GetVar("verificationType"),
        "verificationStatus": 0,  # 1 -> Active: Active policy --- 2 -> Inactive: Inactive policy --- 3 -> Not Found: The patient was not found on Carrier's website, --- 4 -> No Content Loaded: The site did not load an element. --- 5 -> Max out: Active policy, but maxed out. --- 6 -> Termed: The insurance plan has ended. --- 7 -> Unknown: There is no information for a Dental Plan.
        "patientId": int(GetVar("patnum")) if GetVar("patnum") not in ["Empty", "", " "] else GetVar("patnum"),
        "subscriberId": int(GetVar("sub_id")) if GetVar("sub_id") not in ["Empty", "", " "] else GetVar("sub_id"),
        "planId": int(GetVar("plannum")) if GetVar("plannum") not in ["Empty", "", " "] else GetVar("plannum"),
        "planStartDate": str(GetVar("plan_start_date")).split("-")[0].strip() if '-' in GetVar("plan_start_date") else "",
        "appointmentDateTime": GetVar("app_datetime"),
        "employerName": GetVar("employer_name"),
        "priority": int(GetVar("priority")) if GetVar("priority") not in ["Empty", "", " "] else GetVar("priority"),
        "benefitLastVerified": "",#today ,
        "eligibilityLastVerified": "", ##today
        "benefitVerifiedBreakdown": {},
    "rowNumber": GetVar("row_number")
    }]

try:

    if verification_status.split('/')[0].strip() == "Active":
        body[0]["verificationStatus"] = 1
    elif "inactive" in verification_status.lower():
        body[0]["verificationStatus"] = 2
    elif "not found" in verification_status.lower().lower():
        body[0]["verificationStatus"] = 3
    elif "no content loaded" in verification_status.lower():
        body[0]["verificationStatus"] = 4
    elif "max out" in verification_status.lower():
        body[0]["verificationStatus"] = 5
    elif  verification_status.lower() in ["termed", "terminated"]:
        body[0]["verificationStatus"] = 6
    elif "unknown" in verification_status.lower():
        body[0]["verificationStatus"] = 7
    else:
        body[0]["verificationStatus"] = 0
except:
    print(body)
    raise Exception("Review")

class NpEncoder(json.JSONEncoder):
    global np
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

print(json.dumps(body, indent=4, cls=NpEncoder))

response = post(
    url=endpoint,
    json=body,
    headers=headers
)
    
if response.status_code == 200:
    print(f"\nResponse Status Code: {response.status_code}\nResponse Content: {response.content}\n")
    apt_date = GetVar('app_datetime').split(" ")[0].replace("/", "_")
    json_path = f"{GetVar('base_pathP')}/json/body_{GetVar('apt_num')}_{apt_date}.json"
    with open(json_path, "a") as f:
        json.dump(body, f, indent=4)
else:
    print(f"\nERROR\n\nResponse Status Code: {response.status_code}\nResponse Content: {response.content}\n")