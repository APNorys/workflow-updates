import re
import pandas as pd
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

def get_frequencies_patient_chart(plannum, mydb, mycursor):
    
    columns = ["BenefitNum", "Bookmark", "Description", "BenefitType", 
    "TimePeriod", "QuantityQualifier", "Quantity"]
    sql = f"""SELECT benefitnum, 
    case 
	    when be.covcatnum = 0 then CONCAT(code.proccode,"frequency")
	    when be.covcatnum = 3 then "D0120frequency"
    end as "Bookmark",
    code.descript,
    be.benefittype,
    be.timeperiod,
    be.quantityqualifier,
    be.quantity
    FROM benefit be 
    left join covcat cov on be.covcatnum = cov.covcatnum
    left join procedurecode code on be.codenum = code.codenum
    WHERE be.PLANNUM = {plannum} AND be.benefittype = 5 and be.monetaryamt = -1 and be.quantityqualifier <> 2;"""
    mycursor.execute(sql)
    result = mycursor.fetchall()
    result = [list(item) for item in result]
    bookmarks = [item[1] for item in result]
    chart_info = pd.DataFrame(result, columns=columns,
                                        index= bookmarks)
    print(chart_info)

    return chart_info


def translate_values(dataframe):
    status_list = []
    time_period = None
    quantity_qualifier = None  
    
    """**IS_TYPE1**

    - Every # Months

    1 Per 36 Months
    1 TIME IN 6 MONTHS
    Once Per 36 Consecutive Months
    1 Unit, per 36 Months
    1 - F - 36M
    once in 12 consecutive months
    once in 12 consecutive months
    once in any 60 consecutive month period
    once in any 6 consecutive month period
    once per quadrant in any 24 consecutive month period
    Once Per 36 Consecutive Months
    once per tooth in any 36 consecutive month period
    once per quadrant within a 24 month period
    once per tooth in any 36 consecutive month period
    once per quadrant in any 24 consecutive month period
    once per tooth within a 24 month period for teeth without caries
    twice per 12 consecutive months"""
    is_type1 = re.compile(r'(^\d per \d+ months.*)|(^\d+x \d+ mos.*)|(^\d+x \d+mos.*)|1 - f - 36m|\d+ month[s]? interval[s]?$|^\d x \d+ months$|(^\d+/\d+ rolling month$)|(^(one visit|once|\d+|covered|replacement considered).*(every|cleaning per|per|in|f|is).*(\d+)?.*([6]+ months|m|consecutive month period|consecutive months|month period|[\d+] months to the exact day|consecutive months old|on the indicated tooth|additional tooth surface|month period for teeth without caries|36m|24 months|84 months|60 months|60 month on the indicated tooth|5 months|4 months|3 months|mos)$)', re.IGNORECASE)
    
    
    """**IS_TYPE2**

    - # Per Calendar Year
    2 Per Calendar Year
    2 Per Calendar Year
    2 Per Calendar Year
    one of any bitewing x-ray procedure within a calendar year
    two of any bitewing x-ray procedure within a calendar year
    two of any bitewing x-ray procedure within a calendar year
    two of any bitewing x-ray procedure within a calendar year
    2 in a benefit year
    two of any bitewing x-ray procedure within a calendar year for children
    one of any bitewing x-ray procedure within a calendar year for adults
    Twice Per Calendar Year
    1 TIME IN 1 CALENDAR YEAR
    2/plan year
    1 Unit, for 1 Calendar Year
    1 Unit, for 1 Calendar Year
    2 Visits, for 1 Calendar Year
    two fluoride procedures within a calendar year
    2 TIMES IN 1 CALENDAR YEAR
    1 - P - 1Y
    2 - P - 1Y
    two of any prophylaxis procedures within a calendar year for codes D1110, D1120, D4346, D4355, and D4910
    two of any bitewing x-ray procedure within a calendar year
    four films per calendar year
    Once Per Calendar Year
    Twice Per Calendar Year
    four films per calendar year
    two fluoride procedures within a calendar year
    once in a calendar year, subject to all other plan provisions
    one fluoride procedure within a calendar year
    1 in a benefit year. Bitewing benefit for children under 10 limited to two films.
    1 in a benefit year. Bitewing benefit for children under 10 limited to two films.
    one of any bitewing x-ray procedure within a calendar year
    2 Visits, for 1 Calendar Year"""
    is_type2 = re.compile(r'(\d+ - p - 1y)|(^\d+ - f - 12m)|(^\d x annually$)|(^1 per 12 months$)|(^[\d]x per 1 (benefit period|year[s]?)$)|(^periodontal maintenance or regular  adult prophylaxis 2 per calendar year$)|(^for children under age 19 twice per calendar year|^two per year$|^2 per 1 period for children separated by 6 months 1 per 1 period for adults liberal interpretation)|(^2 per 1 calendar years for children 1 per 1 calendar years for adults$)|(^\d/plan year)|(^\d/calendar year)|(^(procedure|once|twice|thrice|\d{1,2}|one|two|four|\d+ unit|\d+ sets).*(within|per|times|for|in|p).*(benefit|1 period for adults|calendar\syear[\.]?|\dy|benefit\syear|calendar year for ages under 12 bitewings are limited to 2 bitewings per occurrence|calendar year for children|calendar year for adults|calendar year for codes d1110 d1120 d4346 d4355 and d4910|calendar year, subject to all other plan provisions|per provider\.|12 consecutive months|benefit year. bitewing benefit for children under 10 limited to two films[\.]?|1y|service year|contract period|benefit period)$)', re.IGNORECASE)
    
    """**IS_TYPE3**

    - # In the last 12 Months
    RARE"""
    is_type3 = re.compile(r'^(\d{1,1}).*(in).*(12 months)', re.IGNORECASE)
    
    """**IS_TYPE4**

    - Every # of Years
    1 TIME IN 5 CALENDAR YEARS
    5 year intervals
    once within a 3 year period from the last date performed
    1 Per 3 Years
    once within a 5 year period from the last date performed
    once within a 3 year period from the last date performed
    once in a five-year period
    once per tooth per lifetime
    once within a 5 year period from the last date performed
    once per lifetime
    Sealants are a benefit on unrestored bicuspids, primary and permanent molars; generally limited to one placement per tooth in a lifetime
    once within a 5 year period from the last date performed
    2 times per tooth in a benefit year
    5 year intervals"""
    is_type4 = re.compile(r'(1 unit for 3 calendar years)|(^\d x \d years$)|(^[\d]x per \d (benefit period|year[s]?)$)|(^3 year intervals generally limited to one placement per tooth  on permanent unrestored 1st and 2nd molars without cavities for dependents to age ages 0-13$)|(^\d/\d calendar year$)|(^(allowed|image|once|one|\d).*(in|every|year|within|per).*(\d|five)?.*(years|year period|intervals|year period from the last date performed[\.]?|year)$)', re.IGNORECASE)
    is_type5 = re.compile(r'^.*lifetime.*|.*99.*|.*999.*|.*9999.*|.*life\stime.*', re.IGNORECASE)
    values_translated = dataframe
    # Drop ambiguous bookmark values
    values_translated = values_translated.applymap(lambda s: s.lower() if type(s) == str else s)
    values_translated = values_translated.drop(values_translated[(values_translated['Bookmark_values'] == 'once per date of service')].index )
    
    print(f'\n{color.YELLOW}{"*"*10} TRANSLATING FREQUENCIES {"*"*10}{color.END}\n')
    for bookmark in values_translated.index.tolist():
        frequency = values_translated.loc[bookmark]["Bookmark_values"]
        frequency = frequency.lower().strip().replace('\n',' ').replace('(s)','').replace('.','').replace(',','').replace('(','').replace(')','').replace(' - 2 remaining','').replace(' - 1 remaining', '').replace(' - allow 1 more if qualifying perio patient', '').replace(' - 0 remaining','')
        frequency = frequency.replace(' in combination with routine cleanings','').replace(' | age 5 and older','').replace(' | 4 per day','').replace(' per dentist','').replace(' 1 additional during pregnancy in combination with periodontal maintenance','')
        frequency = frequency.replace(' under 19 years of age 2 per calendar year following active periodontal therapy age 18 and older','').replace(' per area of the mouth','').replace(' in combination with routine cleanings','').replace(' under 16 years of age permanent 1st and 2nd molars','')
        frequency = frequency.replace(' under 19 years of age permanent 1st molars and primary molars','').replace('tooth per ','').replace(' all teeth under 19 years of age','').replace(' in addition to routine prophylaxis','').replace(' under 14 years of age','')
        frequency = frequency.replace(' 1 additional during pregnancy','').replace('1 per 18 months age 19 and older |','').replace(' under 19 years of age','').replace(' permanent 1st molars and primary molars','').replace(' | no alternate benefit','')
        frequency = frequency.replace('visits every 1 service year','per calendar year').replace('one crown procedure per tooth within a 24 month period','once per 24 months').replace('one crown procedure per provider within a 24 month period','once per 24 months').replace('one crown procedure per tooth within a 3 month period','once per 3 consecutive month period').strip()
        frequency = frequency.replace('once per tooth within a 3 year period for teeth without caries', 'once per 3 consecutive month period').replace('once in any three year period','once every 3 years').replace('one problem focused evaluation within a 30 day period','once per 1 consecutive month period').replace('occurrences per provider within a 12 month period','once per 12 consecutive month period').strip()
        frequency = frequency.replace('to either one d0210 intraoral complete series or d0330 panoramic radiographic image within a 5 year period from the last date performed', 'once every 5 years').replace('one of any oral evaluation procedure within a 6 month period','once per 6 consecutive month period').replace('twice in any 12 consecutive month period','twice per calendar year').strip()
        frequency = frequency.replace('limited to either a maximum of 4 bitewing films or a set 7-8 films of vertical bitewings in one visit once in any 12 consecutive month period', 'once per calendar year').replace(' once in any 6 consecutive month period if no other treatment other than radiographic images is performed in the same visit','once per 6 consecutive month period')
        frequency = frequency.replace('consultations d9310 are covered once per dental specialty in any 12 consecutive month period; covered when no other treatment other than radiographic images is performed during the same visit', 'once per calendar year').replace('once in any 6 consecutive month period if no other treatment other than radiographic images is performed in the same visit', 'once per 6 consecutive month period')
        frequency = frequency.replace('one occlusal guard within a 12 month period', 'once per calendar year').replace('1 unit for 3 service years', 'one every 3 years').replace(' per quadrant','').replace(' following active periodontal treatment','').replace(' from the last date of service of the same treatment','').replace('allowed 2 in 1 year', 'twice per calendar year')
        frequency = frequency.replace('per policy year', 'per calendar year').replace('x 1 benefit period', 'per calendar year').replace('2 per 1 period for children separated by 6 months', '1 per 6 months').replace(' per tooth','').replace(' permanent 1st and 2nd molars','').replace('1 time in 12 months', '1 per 12 months').replace(' - allow 2 more if qualifying perio patient','')
        frequency = frequency.replace('two of any prophylaxis procedures within a calendar year for codes d1110 d1120 and d4346', 'twice per calendar year').replace('two of either d4910 or d4346 within a calendar year prophylaxis procedures are a benefit following active periodontal therapy once a 30 day post-operative period has completed','twice per calendar year')
        frequency = frequency.replace(' | alternate benefit provision will apply','').replace(' permanent molars', '').replace('all teeth','').replace(' all members to any age','').replace(' subject to all other plan provisions','').replace('limited to either a maximum of 4 bitewing films or a set of vertical bitewings in one visit are covered twice in a calendar year','twice in a calendar year').strip()
        frequency = frequency.replace(' for codes D1110 D1120 D4346 D4355 and D4910','').replace(' maximum age 18','').strip().replace('limited to a single film two three or four films are benefits once in a calendar year','once per calendar year').strip().replace('periodontal maintenance prophylaxes are limited to four in a calendar year but the total may not exceed two routine cleanings or a total of four procedures in a calendar year','four per calendar year')
        frequency = frequency.strip().replace('limited to twice in a calendar year with no age limitation', 'twice per calendar year').replace('limited to twice in a calendar year','twice per calendar year').replace('two within 2 calendar years', 'twice per calendar year')
        frequency = frequency.replace('once within a 5 year period from the last date performed when a panoramic radiographic image is taken in conjunction with an intraoral complete series d0210 or any other radiographic image the panoramic radiographic image fee is considered to be included in the fee for the complete series in such a case a separate fee may not be charged to the patient', '1 every 5 years')
        frequency = frequency.replace('from the last date performed when a panoramic radiographic image is taken in conjunction with an intraoral complete series d0210 or any other radiographic image the panoramic radiographic image fee is considered to be included in the fee for the complete series in such a case a separate fee may not be charged to the patient', '').strip()
        frequency = frequency.replace('three of any prophylaxis procedures within a calendar year for codes d1110 d1120 d4346 d4355 and d4910 prophylaxis procedures are a benefit following active periodontal therapy once a 30 day post-operative period has completed', '3 per calendar year').strip().replace('7 year', '1 every 7 years')
        frequency = frequency.replace('two of any prophylaxis procedures within a calendar year for codes d1110 d1120 d4346 d4355 and d4910 prophylaxis procedures are a benefit following active periodontal therapy once a 30 day post-operative period has completed', 'twice per calendar year').replace('four periodontal prophylaxes are payable per benefit year for individuals with a documented history of periodontal disease', 'four per calendar year')
        frequency = frequency.replace('once within a 6 month period recementation of crowns inlays onlays or bridges is included in the fee for the crown inlay onlay or bridge when performed by the same dentist or dental office within six months of the initial placement', '1 per 6 months').strip().replace('two periodontal prophylaxes are payable per calendar year for individuals with a documented history of periodontal disease', 'twice per calendar year')
        frequency = frequency.replace('once within a 5 year period an allowance may be made for core buildup when extensive loss of tooth structure is supported by radiographic images or narrative report or when following root canal treatment', '1 every 5 years').strip().replace('a third prophylaxis is payable per calendar year with a documented history of periodontal disease and a fourth prophylaxis is payable for two consecutive calendar years following periodontal surgery', 'four per calendar year')
        frequency = frequency.replace('limited to one of any bitewing x-ray procedure within a calendar year for adults', 'once per calendar year').strip().replace('covered twice in a calendar year', 'twice per calendar year').replace('covered once in a calendar year', 'once per calendar year').strip().replace('one implant service within a 1 every 5 years period', '1 every 5 years').replace('once per seven-year period', '1 every 7 years')
        frequency = frequency.replace('either one d0210 intraoral complete series or d0330 panoramic radiographic image within 3 calendar years from the last date performed', 'thrice per calendar year').replace('limited to four films per calendar year', 'four per calendar year').replace('1 per quad every 12 months', 'once per 12 months').replace('once in any seven year period', '1 every 7 years')
        frequency = frequency.replace('either one d0210 intraoral complete series or d0330 panoramic radiographic image within a 3 year period from the last date performed', 'once every 3 years').strip().replace('any two oral evaluation procedures within the contract period', 'twice per calendar year').replace('two of any prophylaxis procedures within a contract period for codes d1110 d1120 and d4346', 'twice per calendar year')
        frequency = frequency.replace('four of any prophylaxis procedures within a calendar year for codes d1110 d1120 and d4346 - limited to two of either d4910 or d4346 within a calendar year prophylaxis procedures are a benefit following active periodontal therapy once a 30 day post-operative period has completed', 'four per calendar year').strip().replace('one per 24 months', 'once per 24 months').replace('once within a 1 every 7 years period', '1 every 7 years')
        frequency = frequency.replace('four of any prophylaxis procedures within a calendar year for codes d1110 d1120 and d4346', 'four per calendar year').strip().replace('one of any bitewing x-ray procedure within a six month period', 'once per 6 months').replace('24 consecutive months','once per 24 months').replace('once per tooth within a 24 month period for teeth without caries', 'once per 24 months').replace('one implant service within a 1 every 7 years period', '1 every 7 years')
        frequency = frequency.replace('two of any prophylaxis procedures within the contract period for codes d1110 d1120 d4346 d4355 and d4910', 'twice per calendar year').strip().replace('1 unit calendar year','once per calendar year').replace('any three oral evaluation procedures within a calendar year','thrice per calendar year').replace('once within a 24 month period', 'once per 24 months').replace('once within a 24-month period', 'once per 24 months')
        frequency = frequency.replace('either one d0210 intraoral complete series or d0330 panoramic radiographic image within a 5 year period from the last date performed', 'once every 5 years').replace('2 units calendar year','twice per calendar year').replace('once per tooth within a 3 year period for teeth without caries', 'thrice per calendar year').replace('once per 24 months-', 'once per 24 months').strip().replace('once within a 1 every 5 years period for teeth without caries', '1 every 5 years')
        frequency = frequency.replace('two of any prophylaxis procedures within a 12 month period for codes d1110 d1120 d4346 d4355 and d4910', '1 per 12 months').replace('one of any prophylaxis procedure within a 6 month period for codes d1110 d1120 d4346 d4355 and d4910', 'once per 6 months').replace('two of any oral evaluation procedure within a calendar year - limited to once per provider per lifetime and is included as part of the oral evaluation limitations of your program', 'twice per calendar year')
        frequency = frequency.replace('two fluoride procedures within a 12 month period', '1 per 12 months').replace('four of either d4910 or d4346 within a calendar year prophylaxis procedures are a benefit following active periodontal therapy once a 30 day post-operative period has completed', 'four per calendar year').replace('once per quadrant in 24 consecutive months', 'once per 24 months').replace('thrice per calendar year - once per provider - once per provider per lifetime and is included as part of the oral evaluation limitations of your program', 'thrice per calendar year')
        frequency = frequency.replace('two of any bitewing x-ray procedure within a 12 month period', '1 per 12 months').replace('any four oral evaluation procedures within a calendar year comprehensive evaluations are limited to once per provider', 'four per calendar year').replace('four per 12 month per patient', 'four per calendar year').replace('limited to once in a five year period', '1 every 5 years').replace('two of any oral evaluation procedure within a calendar year - once per provider per lifetime and is included as part of the oral evaluation limitations of your program', 'twice per calendar year')
        frequency = frequency.replace('two of any oral evaluation procedure within a 12 month period', '1 per 12 months').replace('periodontal prophylaxes are payable only for individuals with a documented history of periodontal disease not within 30 days of active therapy and limited to 4 per calendar year', 'four per calendar year').replace('two per 12 month','twice per calendar year').replace('once per 24 months for teeth without caries', 'once per 24 months').replace('two additional prophylaxes are payable per calendar year for individuals with a documented history of periodontal disease', 'twice per calendar year')
        frequency = frequency.replace('either one d0210 intraoral complete series or d0330 panoramic radiographic image within a 5 year period from the last date performed', '1 every 5 years').replace('four of any bitewing x-ray procedure within a 6 month period','once per 6 months').replace('one per 60 month', 'once per 60 months').replace('one per 12 month', 'once per 12 months').replace('four per 12 month per patient per tooth', 'four per 12 months').replace('5 year', '1 every 5 years')
        frequency = frequency.replace('2 per 1 fiscal year for children', 'twice per calendar year').replace('two of any prophylaxis procedures within a calendar year for codes d1110 and d4346', 'twice per calendar year').replace('three of any prophylaxis procedures within a calendar year for codes d1110 d1120 and d4346','thrice per calendar year').replace('six per 12 month', '6 per 12 months').replace('limited to once per three-year period', '1 every 3 years').replace('one full mouth series of intra-oral films within a 1 every 5 years period from the last date performed', '1 every 5 years')
        frequency = frequency.replace('1 per 1 fiscal year for adults', 'once per calendar year').replace('two per 1 year','twice per calendar year').replace('three per 1 year per patient', 'thrice per calendar year').replace('two per 12 month', 'twice per calendar year').replace('two of per 1 year per patient','twice per calendar year').replace('three of per 1 year', 'thrice per calendar year').replace('once per calendar year for age 14 and under and', '').replace('once in once per 24 months', 'once per 24 months')
        frequency = frequency.replace('one per tooth within a 3 year period for teeth without caries', 'thrice per calendar year').strip().replace('1 x 5 year', '1 every 5 years').replace('may not exceed 2 time in 1 calendar year','twice per calendar year').replace('may not exceed 1 time in 60 month', 'once per 60 months').replace('may not exceed 3 time in 1 calendar year', 'thrice per calendar year').replace('may not exceed 1 time in 24 month on the indicated tooth may not exceed 1 time in 2 month on the indicated tooth/surface', 'once per 24 momths').replace('once within a 1 every 5 years period for teeth without caries', '1 every 5 years')

        type1 = is_type1.match(frequency)
        type2 = is_type2.match(frequency)
        type3 = is_type3.match(frequency)
        type4 = is_type4.match(frequency)
        type5 = is_type5.match(frequency)
        print(f'{"_"*110}\n{bookmark} FREQUENCY ==> {frequency}')
        if type5:
            print("Type 5 Match!!!")
            values_translated.at[bookmark, "Quantity"] = 1
            values_translated.at[bookmark, "QuantityQualifier"] = 1
            values_translated.at[bookmark, "TimePeriod"] = 3
            values_translated.at[bookmark, "Type"] = 5
            values_translated.at[bookmark, "FrequencyTranslated"] = '1 Time per Lifetime'
            status_list.append("Found")
            print(f'FREQUENCY TRANSLATED ==> 1 Time per Lifetime')
        elif type1:
            print("Type 1 Match!!!")
            frequency = frequency.replace("twice", "2").replace("once","1").replace("thrice","3")
            frequency = frequency.replace('/',' / ').replace('month','').replace('months','').replace('m','').replace('-','').replace('mos','').replace('x','')
            quantity_list = [int(i) for i in frequency.split() if i.isdigit()]
            print(quantity_list)
            quantity = quantity_list[0] if len(quantity_list) == 1 else (quantity_list[1]//quantity_list[0])  
            values_translated.at[bookmark, "Quantity"] = quantity
            values_translated.at[bookmark, "QuantityQualifier"] = 5
            values_translated.at[bookmark, "TimePeriod"] = 0
            values_translated.at[bookmark, "Type"] = 1
            values_translated.at[bookmark, "FrequencyTranslated"] = f'Every {quantity} Months'
            status_list.append("Found")
            print(f'FREQUENCY TRANSLATED ==> Every {quantity} Months')
        elif type2:
            print("Type 2 Match!!!")
            frequency = frequency.replace("twice", "2").replace("once","1").replace("thrice","3").replace('one','1').replace('two','2').replace('four','4').replace('/', ' / ').replace('age 19','').replace('x','')
            quantity_list = [int(i) for i in frequency.split() if i.isdigit()]
            quantity = quantity_list[0]
            values_translated.at[bookmark, "Quantity"] = quantity
            values_translated.at[bookmark, "QuantityQualifier"] = 1
            values_translated.at[bookmark, "TimePeriod"] = 2
            values_translated.at[bookmark, "Type"] = 2
            values_translated.at[bookmark, "FrequencyTranslated"] = f'{quantity} Per calendar year'
            status_list.append("Found")
            print(f'FREQUENCY TRANSLATED ==> {quantity} Per calendar year')
        elif type3:
            print("Type 3 Match!!!")
            quantity_list = [int(i) for i in frequency.split() if i.isdigit()]
            quantity = quantity_list[0] if len(quantity_list) > 0 else None 
            values_translated.at[bookmark, "Quantity"] = quantity
            values_translated.at[bookmark, "QuantityQualifier"] = 1
            values_translated.at[bookmark, "TimePeriod"] = 5
            values_translated.at[bookmark, "Type"] = 3
            values_translated.at[bookmark, "FrequencyTranslated"] = f'{quantity} In the last 12 months'
            status_list.append("Found")
            print(f'FREQUENCY TRANSLATED ==> {quantity} In the last 12 months')
        elif type4:
            print("Type 4 Match!!!")
            frequency = frequency.replace("twice", "2").replace("once","1").replace("thrice","3").replace('one','1').replace('two','2').replace('three','3').replace('four','4').replace('five','5').replace('-', ' ').replace('/', ' / ')
            quantity_list = [int(i) for i in frequency.split() if i.isdigit()]
            quantity = quantity_list[0] if len(quantity_list) == 1 else (quantity_list[1]//quantity_list[0])  
            values_translated.at[bookmark, "Quantity"] = quantity
            values_translated.at[bookmark, "QuantityQualifier"] = 4
            values_translated.at[bookmark, "TimePeriod"] = 0
            values_translated.at[bookmark, "Type"] = 4
            values_translated.at[bookmark, "FrequencyTranslated"] = f'Every {quantity} Years'
            status_list.append("Found")
            print(f'FREQUENCY TRANSLATED ==> Every {quantity} Years')
        else:
            print(f'{bookmark} - {frequency} doesnt match')
            values_translated.loc[bookmark, "Quantity"] = ""
            print("HERE")
            values_translated.loc[bookmark, "QuantityQualifier"] = ""
            values_translated.loc[bookmark, "TimePeriod"] = ""
            values_translated.loc[bookmark, "Type"] = "Not Found"
            values_translated.loc[bookmark, "FrequencyTranslated"] = ""

    return values_translated


def frequency_exist_in_od(mycursor, plannum,  dataframe):
    for bookmark in dataframe.index.tolist():
        codenum = dataframe.loc[bookmark, "CodeNum"]
        covcatnum = dataframe.loc[bookmark, "CovCatNum"]
        frequency_type = dataframe.loc[bookmark, "Type"]
        
        sql = f""" SELECT quantity, quantityqualifier, TimePeriod FROM benefit WHERE benefittype = 5 and quantityqualifier <> 2
            AND plannum = {plannum} AND  codenum = {codenum} AND covcatnum = {covcatnum}"""
        print(sql)
        mycursor.execute(sql)
        result = mycursor.fetchone()
        if result:
            dataframe.at[bookmark, "Exist"] = True
            old_quantity = result[0]
            if frequency_type == 1:
                dataframe.loc[bookmark, "OldValue"] = f'Every {old_quantity} months'
            elif frequency_type == 2:
                dataframe.loc[bookmark, "OldValue"] = f'{old_quantity} per calendar year'
            elif frequency_type == 3:
                dataframe.loc[bookmark, "OldValue"] = f'{old_quantity} in the last 12 months'
            elif frequency_type == 4:
                dataframe.loc[bookmark, "OldValue"] = f'Every {old_quantity} years'
            elif frequency_type == 5:
                dataframe.loc[bookmark, "OldValue"] = f'1 Time per Lifetime'
        else:
            dataframe.loc[bookmark, "Exist"] = False
            dataframe.loc[bookmark, "OldValue"] = "-"

    return dataframe

def update_frequencies(mydb, mycursor, dataframe):
    try:
        for bookmark in dataframe.index.tolist():
            quantity = dataframe.loc[bookmark]["Quantity"]
            quantity_qualifier = dataframe.loc[bookmark]["QuantityQualifier"]
            time_period = dataframe.loc[bookmark]["TimePeriod"]
            benefit_num = dataframe.loc[bookmark]["BenefitNum"]
            print(quantity)
            if quantity:
                sql = f"""UPDATE benefit SET quantity = {quantity}, quantityqualifier = {quantity_qualifier}, 
                            timeperiod = {time_period} WHERE benefitnum = {benefit_num}"""
                print(f'UPDATE FREQUENCY QUERY ==> {sql}')
                mycursor.execute(sql)
                mydb.commit()
                print(f"{mycursor.rowcount} Row(s) Affected")
            else:
                print(f'EMPTY VALUE FOR FREQUENCY, QUANTITY ==> {quantity}')
    except Exception as e:
        print(f'ERROR WHILE UPDATING FREQUENCIES {e}')

def insert_frequencies(mydb, mycursor,plannum, dataframe):
    try:
        for bookmark in dataframe.index.tolist():
            quantity = dataframe.loc[bookmark]["Quantity"]
            quantity_qualifier = dataframe.loc[bookmark]["QuantityQualifier"]
            time_period = dataframe.loc[bookmark]["TimePeriod"]
            covcatnum = dataframe.loc[bookmark]["CovCatNum"]
            code_num = dataframe.loc[bookmark]["CodeNum"]
            exist = dataframe.loc[bookmark]["Exist"]
            if quantity and not exist:
                sql = f"""INSERT INTO benefit (plannum,patplannum,covcatnum,benefittype,percent,monetaryamt,
                timeperiod,quantityqualifier,quantity,codenum,coveragelevel,secdatetentry) 
                VALUES({plannum},0,{covcatnum},5,-1,'-1.00',{time_period},{quantity_qualifier},{quantity},{code_num},0,NOW())"""
                print(f'INSERT FREQUENCY QUERY ==> {sql}')
                mycursor.execute(sql)
                mydb.commit()
                print(f"{mycursor.rowcount} Row(s) Affected")

            elif quantity and exist:
                sql = f"""UPDATE benefit SET quantity = {quantity}, quantityqualifier = {quantity_qualifier}, 
                            timeperiod = {time_period} WHERE benefittype = 5 AND codenum = {code_num} and quantityqualifier <> 2
                            AND covcatnum = {covcatnum} and plannum = {plannum}"""
                print(f'UPDATE FREQUENCY QUERY ==> {sql}')
                mycursor.execute(sql)
                mydb.commit()
                print(f"{mycursor.rowcount} Row(s) Affected")
            else:
                print(f'EMPTY VALUE FOR FREQUENCY {bookmark}, QUANTITY ==> {quantity}')
    except Exception as e:
        print(f'ERROR WHILE UPDATING FREQUENCIES {e}')

def get_frequencies_patient_by_code(codenums, plannum, mycursor):
    columns = ["BenefitNum", "Bookmark", "Description", "BenefitType", "TimePeriod", "QuantityQualifier", "Quantity"]
    sql = f"""SELECT
        benefitnum, 
        CASE 
            WHEN be.covcatnum = 0 THEN CONCAT(code.proccode,"frequency")
            WHEN be.covcatnum = 3 THEN "D0120frequency"
        END AS "Bookmark",
        code.descript,
        be.benefittype,
        be.timeperiod,
        be.quantityqualifier,
        be.quantity
        FROM benefit be 
        LEFT JOIN covcat cov ON be.covcatnum = cov.covcatnum
        LEFT JOIN procedurecode code ON be.codenum = code.codenum
        WHERE be.plannum = {plannum} AND be.codenum IN ({codenums}) AND be.benefittype = 5 AND
        be.monetaryamt = -1 AND be.quantityqualifier <> 2;"""
    mycursor.execute(sql)
    result = mycursor.fetchall()
    result = [list(item) for item in result]
    bookmarks = [item[1] for item in result]
    chart_info = pd.DataFrame(result, columns=columns, index= bookmarks)
    print(chart_info)

    return chart_info

def translate_from_pms_values(dataframe):
    """
    - Every # Months
        TimePeriod  QuantityQualifier
            0               5

    - # Per Calendar Year
        TimePeriod  QuantityQualifier
            2               1

    - # In the last 12 Months
        TimePeriod  QuantityQualifier
            5               1

    - Every # of Years
        TimePeriod  QuantityQualifier
            0               4
    """

    if not dataframe[(dataframe['TimePeriod'] == 0) & (dataframe['QuantityQualifier'] == 5)].empty:
        return f"Every {dataframe['Quantity'].values[0]} Months"

    elif not dataframe[(dataframe['TimePeriod'] == 2) & (dataframe['QuantityQualifier'] == 1)].empty:
        if dataframe['Quantity'].values[0] == 1:
            return "Once Per Calendar Year"
        else:
            return f"{dataframe['Quantity'].values[0]} Per Calendar Year" 

    elif not dataframe[(dataframe['TimePeriod'] == 5) & (dataframe['QuantityQualifier'] == 1)].empty:
        return f"{dataframe['Quantity'].values[0]} In the last 12 Months"

    elif not dataframe[(dataframe['TimePeriod'] == 0) & (dataframe['QuantityQualifier'] == 4)].empty:
        return f"Every {dataframe['Quantity'].values[0]} of Years"

    else:
        print('Check. Type not found.')