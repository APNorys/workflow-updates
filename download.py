DOWNLOAD_DICT = {
    "GITLAB_TOKEN": "glpat-iSVsoi6qPbLxtxrPp7pf",
    "LITE":{
            "extraction": {
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/insurance-verification-products/-/archive/main/insurance-verification-products-main.zip?path=LITE/Extraction",
                "SIBLING_BOTS":["Insurance_verification_Lite_version.json"],
                "DOWNLOAD_NAME":"extraction.zip",
                "DOWNLOAD_PATH":"insurance-verification-products-main-LITE-Extraction/LITE/Extraction"
            },
            "writeback": {
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/insurance-verification-products/-/archive/main/insurance-verification-products-main.zip?path=LITE/Writeback",
                "SIBLING_BOTS":[
                    "Insurance_Verification_Upload_Lite_version.json",
                    "Pre_Data_Writeback_version.json"
                    ],
                "DOWNLOAD_NAME":"writeback.zip",
                "DOWNLOAD_PATH":"insurance-verification-products-main-LITE-Writeback/LITE/Writeback"
            }
        },
    "MODS": {
            "OpenDental": {
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/open-dental-architeture/-/archive/master/open-dental-architeture-master.zip?path=Open_Dental_Module",
                "DOWNLOAD_NAME":"OpenDental.zip",
                "DOWNLOAD_PATH":"open-dental-architeture-master-Open_Dental_Module",
                "GITLAB_MOD_NAME": "Open_Dental_Module",
                "MOD_NAME": "OpenDental",
                "ID_GIT" : 25869575
            },
            "IV_Workflow": {
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/open-dental-architeture/-/archive/master/open-dental-architeture-master.zip?path=IV_Workflow",
                "DOWNLOAD_NAME":"IV_Workflow.zip",
                "DOWNLOAD_PATH":"open-dental-architeture-master-IV_Workflow",
                "GITLAB_MOD_NAME": "IV_Workflow",
                "MOD_NAME":"iv_workflow",
                "ID_GIT" : 25869575
            },
            "Workflow-Google":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/open-dental-architeture/-/archive/master/open-dental-architeture-master.zip?path=WorkFlow-Google",
                "DOWNLOAD_NAME":"WorkFlowGoogle.zip",
                "DOWNLOAD_PATH":"open-dental-architeture-master-WorkFlow-Google",
                "GITLAB_MOD_NAME": "WorkFlow-Google",
                "MOD_NAME":"Workflow-Google",
                "ID_GIT" : 25869575
            },
            "Workflow-Updates":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/open-dental-architeture/-/archive/master/open-dental-architeture-master.zip?path=Workflow-Updates",
                "DOWNLOAD_NAME":"WorkflowUpdates.zip",
                "DOWNLOAD_PATH":"open-dental-architeture-master-Workflow-Updates",
                "GITLAB_MOD_NAME": "Workflow-Updates",
                "MOD_NAME":"Workflow-Updates",
                "ID_GIT" : 25869575
            },
            "CarriersAPITools":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/carriers-2.0/modules/CarriersAPITools/-/archive/main/CarriersAPITools-main.zip",
                "DOWNLOAD_NAME":"CarriersAPITools.zip",
                "DOWNLOAD_PATH":"CarriersAPITools-main",
                "GITLAB_MOD_NAME": "",
                "MOD_NAME":"CarriersAPITools",
                "ID_GIT" : 36489532
            },
            "Metrics":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/carriers-2.0/modules/metrics/-/archive/main/metrics-main.zip",
                "DOWNLOAD_NAME":"metrics.zip",
                "DOWNLOAD_PATH":"metrics-main",
                "GITLAB_MOD_NAME": "",
                "MOD_NAME":"Metrics",
                "ID_GIT" : 36491996
            },
            
            "Dentalrobot":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/carriers-2.0/modules/DentalRobot/-/archive/main/DentalRobot-main.zip",
                "DOWNLOAD_NAME":"Dentalrobot.zip",
                "DOWNLOAD_PATH":"DentalRobot-main",
                "GITLAB_MOD_NAME": "",
                "MOD_NAME":"DentalRobot",
                "ID_GIT" : 36491956
            },
            "PlanChecker":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/carriers-2.0/modules/PlanChecker/-/archive/main/PlanChecker-main.zip",
                "DOWNLOAD_NAME":"PlanChecker.zip",
                "DOWNLOAD_PATH":"PlanChecker-main",
                "GITLAB_MOD_NAME": "",
                "MOD_NAME":"PlanChecker",
                "ID_GIT" : 37380467
            },
            "dentalrobot-lib":{
                "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/open-dental-architeture/-/archive/master/open-dental-architeture-master.zip?path=dentalrobot",
                "DOWNLOAD_NAME":"dentalrobot.zip",
                "DOWNLOAD_PATH":"open-dental-architeture-master-dentalrobot",
                "GITLAB_MOD_NAME": "dentalrobot",
                "MOD_NAME":"dentalrobot",
                "ID_GIT" : 25869575
            }
    },
    "RESOURCES":{
            "URL":"https://gitlab.com/ralop-tech/rocketbot/dentalrobot-library/insurance-verification-products/-/archive/main/insurance-verification-products-main.zip?path=Resources",
            "SIBLINGS":[
                    "Eligibility_print.json",
                    "new_state_setter.json",
                    "returned_values_template.json"
                    ],
            "DOWNLOAD_NAME":"Resources.zip",
            "DOWNLOAD_PATH":"insurance-verification-products-main-Resources/Resources"
            }
    }
